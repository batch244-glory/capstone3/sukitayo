import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
//import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Create() {
	const {user} = useContext(UserContext);

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [isActive, setIsActive] = useState(false);

	function createProduct(e) {
		e.preventDefault();

		if (!user.isAdmin) {
		  Swal.fire({
		    title: "Authentication failed",
		    icon: "error",
		    text: "You are not an admin!"
		  })
		  return;
		}

		fetch(`${process.env.REACT_APP_API_URL}/products/create`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price				
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			// if(data.access !== undefined) {
			// 	localStorage.setItem('token', data.access);
			// 	retrieveUserDetails(data.access);

			// 	Swal.fire({
			// 		title: "Product Created",
			// 		icon: "success",
			// 		text: "Product Created Successfully!"
			// 	});
			// } else {
			// 	Swal.fire({
			// 		title: "Authentication failed",
			// 		icon: "error",
			// 		text: "You are not an admin!"
			// 	})
			// }

			setName('');
			setDescription('');
			setPrice('');
		})
	}

	useEffect(() => {
		if((name !== '' && description !== '' && price !== '')) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [name, description, price])

	return (
		<Form onSubmit={(e) => createProduct(e)}>
			<Form.Group controlId = "productName">
				<Form.Label>Name</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter productName"
					required
					value={name}
					onChange={e => setName(e.target.value)}
				/>
			</Form.Group>

			<Form.Group controlId = "productDescription">
				<Form.Label>Description</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter productDescription"
					required
					value={description}
					onChange={e => setDescription(e.target.value)}
				/>
			</Form.Group>

			<Form.Group controlId = "productPrice">
				<Form.Label>Price</Form.Label>
				<Form.Control
					type="number"
					placeholder="Enter productPrice"
					required
					value={price}
					onChange={e => setPrice(e.target.value)}
				/>
			</Form.Group>

			{isActive
				?
					<Button variant="primary" type="submit" id="submitBtn">Submit</Button>
				:
					<Button variant="danger" type="submit" id="submitBtn" disabled>Submit</Button>
			}

		</Form>
	)
}