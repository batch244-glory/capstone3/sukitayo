import { useContext, useState } from 'react';
import UserContext from '../UserContext';
import { Link, useNavigate } from 'react-router-dom';
import { Card, Button } from 'react-bootstrap';

export default function ProductCard({ productProp }) {
  //const { productId } = useParams();
  const { user } = useContext(UserContext);
  const { _id, name, description, price, isActive } = productProp;
  const navigate = useNavigate();

  return (
    <Card>
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle>Description:</Card.Subtitle>
        <Card.Text>{description}</Card.Text>
        <Card.Subtitle>Price:</Card.Subtitle>
        <Card.Text>Php {price}</Card.Text>
        {user.isAdmin ? (
          <>
            <Card.Subtitle>Status:</Card.Subtitle>
            <Card.Text>{isActive ? 'Active' : 'Inactive'}</Card.Text>
            <Button className="m-2 p-2" as={Link} to={`/${_id}/editProduct`}>
              Edit
            </Button>
            {isActive ? (
              <Button
                className="m-2 p-2"
                variant="danger"
                as={Link} to={`/${_id}/archive`}
              >
                Archive
              </Button>
            ) : (
              <Button
                className="m-2 p-2"
                as={Link} to={`/${_id}/archive`}
              >
                Unarchive
              </Button>
            )}
          </>
        ) : (
          <>
            <Button className="m-2 p-2" as={Link} to={`/${_id}`} variant="primary">
              Details
            </Button>
            <Button
              className="m-2 p-2"
              as={Link}
              to={`/${_id}/checkout`}
              variant="primary"
            >
              Buy Now
            </Button>
          </>
        )}
      </Card.Body>
    </Card>
  );
}



// import { useContext } from 'react';
// import UserContext from '../UserContext';
// import { Link } from 'react-router-dom';
// import { Card, Button } from 'react-bootstrap';


// export default function ProductCard({productProp}) {
//   	const { user } = useContext(UserContext);
// 	// Deconstruct the course properties into their own variables
// 	const { _id, name, description, price, isActive } = productProp;

// 	return (
// 	    <Card>
// 	        <Card.Body>
// 	            <Card.Title>{name}</Card.Title>
// 	            <Card.Subtitle>Description:</Card.Subtitle>
// 	            <Card.Text>{description}</Card.Text>
// 	            <Card.Subtitle>Price:</Card.Subtitle>
// 	            <Card.Text>Php {price}</Card.Text>
// 	            {user.isAdmin ?
// 	            	<>
// 	            	<Card.Subtitle>Status:</Card.Subtitle>
// 	            	<Card.Text>{isActive ? 'Active' : 'Inactive'}</Card.Text>
// 	                <Button className="m-2 p-2" as={Link} to={`/${_id}/editProduct`}>Edit</Button>
// 	                </>
// 	                 :
// 	                <>
// 	                <Button className="m-2 p-2" as={Link} to={`/${_id}`}variant="primary">Details</Button>
// 	                <Button className="m-2 p-2" as={Link} to={`/products/${_id}`}variant="primary">Buy Now</Button>
// 	                </>
// 	            }
// 	        </Card.Body>
// 	    </Card>
// 	);
// }