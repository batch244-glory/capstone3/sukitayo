const data = {
	products: [
		{
			name: 'Nike Shirt',
			slug: 'nike-shirt',
			category: 'Shirts',
			image:'/images/nikeshirt.jpg',
			price: 250,
			countInStock: 50,
			brand: 'Nike',
			rating: 4.5,
			numReviews: 10,
			description: 'nike quality shirt'
		},
		{
			name: 'Nike Pants',
			slug: 'nike-pants',
			category: 'Shirts',
			image:'/images/nikepants.jpg',
			price: 250,
			countInStock: 50,
			brand: 'Nike',
			rating: 4.5,
			numReviews: 10,
			description: 'nike quality shirt'
		},
		{
			name: 'Adidas Shirt',
			slug: 'adidas-shirt',
			category: 'Shirts',
			image:'/images/adidasshirt.jpg',
			price: 250,
			countInStock: 50,
			brand: 'Nike',
			rating: 4.5,
			numReviews: 10,
			description: 'nike quality shirt'
		},
		{
			name: 'Adidas Pants',
			slug: 'adidas-pants',
			category: 'Shirts',
			image:'/images/adidaspants.jpg',
			price: 250,
			countInStock: 50,
			brand: 'Nike',
			rating: 4.5,
			numReviews: 10,
			description: 'nike quality shirt'
		},
	]
};

export default data;