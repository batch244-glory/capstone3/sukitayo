import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import AppNavbar from './components/AppNavbar';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Home from './pages/Home';
import Admin from './pages/Admin';
import Products from './pages/Products';
import CreateProduct from './pages/CreateProduct';
import ShopView from './pages/ShopView';
import ProductView from './pages/ProductView';
import EditProduct from './pages/EditProduct';
import Archive from './pages/Archive';
import Checkout from './pages/Checkout';
import './App.css';
import { UserProvider } from './UserContext';

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear()
  }

  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  }, [user])

  return(
    <UserProvider value= {{user, setUser, unsetUser}}>

      <Router>
          <AppNavbar/>
          <Container>
              <Routes>
                  <Route path="/" element={<Home/>} />
                  <Route path="/login" element= {<Login/>} />
                  <Route path="/register" element= {<Register/>} />
                  <Route path="/logout" element= {<Logout/>} />
                  <Route path="/admin" element= {<Admin/>} />
                  <Route path="/products" element= {<Products/>} />
                  <Route path="/createProduct" element= {<CreateProduct/>} />
                  <Route path="/products" element= {<Products/>} />
                  <Route path="/shopView" element= {<ShopView/>} />
                  <Route path="/:productId/editProduct" element= {<EditProduct/>} />
                  <Route path="/:productId" element= {<ProductView/>} />
                  <Route path="/:productId/archive" element= {<Archive/>} />
                  <Route path="/:productId/checkout" element= {<Checkout/>} />
              </Routes>
          </Container>
      </Router>
    </UserProvider>
  )
}




export default App;
